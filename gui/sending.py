import socket

class PythonClient():
    def __init__(self, imagePath):
        HOST = '127.0.0.1'
        PORT = 8889
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_address = (HOST, PORT)
        self.sock.connect(self.server_address)
        self.sending(imagePath)
    
    def sending(self, image):
        try:
            # open image
            myfile = open(image, 'rb')
            imageBytes = myfile.read()
            size = len(imageBytes)
            print("Image Size %d" % size)

            # send image size to server
            self.sock.sendall(str(size))
            answer = self.sock.recv(4096)

            print 'answer = %s' % answer

            # send image to server
            if answer == 'ACK_':
                self.sock.sendall(imageBytes)
                # check what server send
                answer = self.sock.recv(4096)
                print 'answer 2 = %s' % answer

                # if answer == 'ACK_IMAGE' :
                    # sock.sendall("BYE BYE ")
                print 'Image successfully send to server'
            elif answer == 'NACK':
                print 'NACK ERROR'
                self.sock.close()
            myfile.close()

        finally:
            self.sock.close()


def main():
    image = "photo.png"

    # Create the Python Client for sending 
    program = PythonClient(image)

    # Start the GUI event loop
    # program.window.mainloop()


if __name__ == '__main__':
    main()