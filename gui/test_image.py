#!/usr/bin/env python

import random
import socket, select
import time
from random import randint

image = "photo.png"

HOST = '127.0.0.1'
PORT = 8889

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (HOST, PORT)
sock.connect(server_address)
try:

    # open image
    myfile = open(image, 'rb')
    imageBytes = myfile.read()
    # print(bytearray(imageBytes))
    size = len(imageBytes)
    print("Image Size %d" % size)

    # send image size to server
    sock.sendall(str(size))
    answer = sock.recv(4096)

    print 'answer = %s' % answer

    # send image to server
    if answer == 'ACK_':
        sock.sendall(imageBytes)
        # time.sleep(0.2)
        # check what server send
        answer = sock.recv(4096)
        print 'answer 2 = %s' % answer

        # if answer == 'ACK_IMAGE' :
            # sock.sendall("BYE BYE ")
        print 'Image successfully send to server'
    elif answer == 'NACK':
        print 'NACK ERROR'
        sock.close()
    myfile.close()

finally:
    sock.close()
