from Tkinter import *
import socket

class MyGui:
    def __init__(self):
        # Create Window 
        self.window = Tk()
        # Window Title
        self.window.title("Image Manager")

        #Set the screen size
        self.window.geometry("640x480")
        self.window.resizable(0, 0) #Don't allow resizing in the x or y direction

        self.createWidgets()
    
    def createWidgets(self):
        self.mainMenu = Frame(self.window)
        self.menuOptions = Frame(self.window, bg = "white")
        self.userAccount = Frame(self.window)
        self.fillingFrame()
    
    def menuOptionsProps(self, menu):
        # Menu Options Frame 
        # def mainMenu(title):
        # frameLabel = Label(window, text=title)
        self.destroy(self.menuOptions)
        if menu == 'Search':
            self.searchOptionsProps(menu)
        elif menu == 'Upload':        
            self.uploadOptionsProps(menu)
        else:
            self.defaultOptionsProps(menu)
    
    def mainMenuProps(self):
        self.menuOptionsProps('default')
        #Define Main Menu
        menu = ["Search", "Upload"]
        bg_colour = '#' + "".join('000000')
        for i in range(len(menu)):
            l = Button(self.mainMenu, 
                text = menu[i], 
                fg = 'White', 
                bg = bg_colour, command = lambda i=i: self.getFunction(menu[i]))
            l.place(x = 10, y = 0 + i * 50, width = 120, height = 50)
        self.mainMenu.place(x = 10, y = 60, width = 200, height = 140)
    
    def userAccountProps(self):
        username = Label(self.userAccount, text = "My Account")
        username.place(x = 30, y = 0)
        self.userAccount.place(x = 500, y = 10, width = 120, height = 30)
    
    def fillingFrame(self):
        self.mainMenuProps()
        # default
        self.userAccountProps()

    def defaultOptionsProps(self, title):
        # Menu Options Frame      
        titleFrameLabel = Label(self.menuOptions, text=title)
        titleFrameLabel.place(x = 200, y = 40, anchor="center")
        self.menuOptions.place(x = 150, y = 50, width=400, height=400)

    def searchOptionsProps(self, title):
        # Menu Options Frame 
        # def mainMenu(title):
        # frameLabel = Label(window, text=title)
        titleFrameLabel = Label(self.menuOptions, text=title)
        titleFrameLabel.place(x = 200, y = 40, anchor="center")

        # Define entries
        imageName = StringVar()
        imageEntry = Entry(self.menuOptions, textvariable = imageName)
        imageEntry.place(x = 60, y = 100)

        searchButton = Button(self.menuOptions, text = "Search", width = 12)
        searchButton.place(x = 40, y = 350)

        self.menuOptions.place(x = 150, y = 50, width=400, height=400)   
    
    def uploadOptionsProps(self, title):
        titleFrameLabel = Label(self.menuOptions, text=title)
        titleFrameLabel.place(x = 200, y = 40, anchor="center")

        # Define entries
        imageName = StringVar()
        self.imageEntry = Entry(self.menuOptions, textvariable = imageName)
        self.imageEntry.place(x = 60, y = 100)
        uploadButton = Button(self.menuOptions, text="Browse", command = lambda: self.browseImages())
        uploadButton.place(x = 60, y = 150)
            

        searchButton = Button(self.menuOptions, text = "Search", width = 12, command = lambda: self.uploading())
        searchButton.place(x = 40, y = 350)

        self.menuOptions.place(x = 150, y = 50, width=400, height=400)
    
    def destroy(self, infoFrame):
        for child in infoFrame.winfo_children():
            child.destroy()

    def browseImages(self):
        from tkFileDialog import askopenfile
        imageName = askopenfile()
        self.imageEntry.delete(0, END)
        self.imageEntry.insert(0, imageName.name)
        self.imageEntry.config(state='readonly')

    def uploading(self):
        # Create the Python Client for sending 
        image = self.imageEntry.get()
        print(image)
        PythonClient(image)

    def getFunction(self, menu):
        if menu:
            return self.menuOptionsProps(menu)


class PythonClient():
    def __init__(self, imagePath):
        HOST = '127.0.0.1'
        PORT = 8889
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_address = (HOST, PORT)
        self.sock.connect(self.server_address)
        self.sending(imagePath)
    
    def sending(self, image):
        try:
            # open image
            myfile = open(image, 'rb')
            imageBytes = myfile.read()
            size = len(imageBytes)
            print("Image Size %d" % size)

            # send image size to server
            self.sock.sendall(str(size))
            answer = self.sock.recv(4096)

            print 'answer = %s' % answer

            # send image to server
            if answer == 'ACK_':
                self.sock.sendall(imageBytes)
                # check what server send
                answer = self.sock.recv(4096)
                print 'answer 2 = %s' % answer

                # if answer == 'ACK_IMAGE' :
                    # sock.sendall("BYE BYE ")
                print 'Image successfully send to server'
            elif answer == 'NACK':
                print 'NACK ERROR'
                self.sock.close()
            myfile.close()

        finally:
            self.sock.close()


def main():
    # Create the entire GUI program
    program = MyGui()

    # Start the GUI event loop
    program.window.mainloop()


if __name__ == '__main__':
    main()

# C++ generates Hash to save in DB



