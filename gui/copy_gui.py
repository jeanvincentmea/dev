from Tkinter import *

def main():
    # Create Window 
    window = Tk()
    # Window Title
    window.title("Image Manager")

    #Set the screen size
    window.geometry("640x480")
    window.resizable(0, 0) #Don't allow resizing in the x or y direction

    mainMenu = Frame(window)
    menuOptions = Frame(window, bg = "white")
    userAccount = Frame(window)
    fillingFrame(mainMenu, menuOptions, userAccount)

    window.mainloop()

def defaultOptionsProps(title, menuOptions):
    # Menu Options Frame 
        
    titleFrameLabel = Label(menuOptions, text=title)
    titleFrameLabel.place(x = 200, y = 40, anchor="center")

    menuOptions.place(x = 150, y = 50, width=400, height=400)

def searchOptionsProps(title, menuOptions):
    # Menu Options Frame 
    # def mainMenu(title):
    # frameLabel = Label(window, text=title)
    titleFrameLabel = Label(menuOptions, text=title)
    titleFrameLabel.place(x = 200, y = 40, anchor="center")

    # Define entries
    imageName = StringVar()
    imageEntry = Entry(menuOptions, textvariable = imageName)
    imageEntry.place(x = 60, y = 100)

    searchButton = Button(menuOptions, text = "Search", width = 12)
    searchButton.place(x = 40, y = 350)

    menuOptions.place(x = 150, y = 50, width=400, height=400)   

def browseImages(imageEntry):
    from tkFileDialog import askopenfile
    imageName = askopenfile()
    imageEntry.delete(0, END)
    imageEntry.insert(0, imageName.name)
    imageEntry.config(state='readonly')


def uploadOptionsProps(title, menuOptions):
    titleFrameLabel = Label(menuOptions, text=title)
    titleFrameLabel.place(x = 200, y = 40, anchor="center")

    # Define entries
    imageName = StringVar()
    imageEntry = Entry(menuOptions, textvariable = imageName)
    imageEntry.place(x = 60, y = 100)

    uploadButton = Button(menuOptions, text="Browse", command = lambda: browseImages(imageEntry))
    uploadButton.place(x = 60, y = 150)

    searchButton = Button(menuOptions, text = "Search", width = 12)
    searchButton.place(x = 40, y = 350)

    menuOptions.place(x = 150, y = 50, width=400, height=400)

def getFunction(menu, menuOptions):
    if menu:
        return menuOptionsProps(menu, menuOptions)

def destroy(infoFrame):
    for child in infoFrame.winfo_children():
        child.destroy()

def mainMenuProps(mainMenu, menuOptions):
    menuOptionsProps('default', menuOptions)
    #Define Main Menu
    menu = ["Search", "Upload"]
    bg_colour = '#' + "".join('000000')
    for i in range(len(menu)):
        l = Button(mainMenu, 
            text = menu[i], 
            fg = 'White', 
            bg = bg_colour, command = lambda i=i: getFunction(menu[i], menuOptions))
        l.place(x = 10, y = 0 + i * 50, width = 120, height = 50)
    mainMenu.place(x = 10, y = 60, width = 200, height = 140)

def menuOptionsProps(menu, menuOptions):
    # Menu Options Frame 
    # def mainMenu(title):
    # frameLabel = Label(window, text=title)
    destroy(menuOptions)
    if menu == 'Search':
        searchOptionsProps(menu, menuOptions)
    elif menu == 'Upload':        
        uploadOptionsProps(menu, menuOptions)
    else:
        defaultOptionsProps(menu, menuOptions)

def userAccountProps(userAccount):
    username = Label(userAccount, text = "My Account")
    username.place(x = 30, y = 0)
    userAccount.place(x = 500, y = 10, width = 120, height = 30)

def fillingFrame(mainMenu, menuOptions, userAccount):
    mainMenuProps(mainMenu, menuOptions)
    # default
    userAccountProps(userAccount)


if __name__ == '__main__':
    main()

# C++ generates Hash to save in DB



