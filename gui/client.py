import socket
import requests

def setPathAndHeaders():
    addr = 'http://localhost:5000'
    URL = addr + '/api/test'

    # prepare headers for http request
    content_type = 'image/jpeg'
    headers = {'content-type': content_type}
    return addr, headers

def post_image(img_file):
    """ post image and return the response """
    URL, headers = setPathAndHeaders()
    img = open(img_file, 'rb').read()
    response = requests.post(URL, data=img, headers=headers)
    return response

def client_program():
    # get the hostname
    host = socket.gethostname()
    ip = socket.gethostname()
    port = 5000  # initiate port no above 1024

    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # get instance
    # look closely. The bind() function takes tuple as argument
    client.connect((ip, port))  # bind host address and port together
    client.send("disconnect")
    print client.recv(1024)
    

if __name__ == '__main__':
    post_image('photo.png')
