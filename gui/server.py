import socket

def server_program():
    # get the hostname
    host = socket.gethostname()
    ip = socket.gethostname()
    port = 5000  # initiate port no above 1024

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # get instance
    # look closely. The bind() function takes tuple as argument
    server.bind((ip, port))  # bind host address and port together

    # configure how many client the server can listen simultaneously
    server.listen(1)
    print "[.] Started Listening on ", ip, ":", port
    
    client, address = server.accept()  # accept new connection
    print "[.] Get a connection from: " + str(address)
    continued = True
    while continued:
        # receive data stream. it won't accept data packet greater than 1024 bytes
        data = client.recv(1024)
        print "Processing data"
        if data:
            print data
            client.send("Hello Client")
            # client.close()   # close the connection
            continued = False


if __name__ == '__main__':
    server_program()
