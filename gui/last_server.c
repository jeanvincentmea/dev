/*
	C socket server example
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>	//strlen strtok
#include<sys/socket.h>
#include<arpa/inet.h>	//inet_addr
#include<unistd.h>	//write
#include<fcntl.h>	//fcntl

#define PORTNUMBER 8888
#define MAXBUF 40000
#define TO_SEC 1
#define TO_USEC 50000

void adx_store_data(const char *filepath, const char *data)
{
    FILE *fp = fopen(filepath, "wb");
    if (fp != NULL)
    {
        fputs(data, fp);
        fclose(fp);
    }
}
/*char temp[1024*1024];
               while (!found) {
                   n = recv(GetSocketId(), &temp[total], sizeof(temp) - total - 1, 0);
                   if (n == -1) {
                        fprintf(stderr, "Error recv data %d\n", errno);
                        return LS_RESULT_FAILED;
 
                   }
                   total += n;
                   temp[total] = '\0';
                   found = (strchr(temp, '!') != 0);
               }*/
/*
// send
char buff[10000];
size_t len = strlen(buff);
char *p = buff;
ssize_t n;
while ( len > 0 && (n=send(sock,p,len,0)) > 0 ) {
  p += n;
  len =- (size_t)n;
}
if ( len > 0 || n < 0 ) {
  // oops, something went wrong
}
 
 
// recv
char buff[10000];
size_t len = sizeof(buff);
char *p = buff;
ssize_t n;
while ( len > 0 && (n=recv(sock,p,len,0)) > 0 ) {
  p += n;
  len =- (size_t)n;
}
if ( len > 0 || n < 0 ) {
  // oops, something went wrong
}
*/
/*
int receive_basic(int s_fd) // Block on recv
{
	int rcvBytes, loopRcvd = 0, totalRcvd = 0;
	char chunk[MAXBUF];

	struct timeval tv = {TO_SEC, TO_USEC};
	setsockopt(s_fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	while(1)
	{
		memset(chunk, 0, MAXBUF);
		if ((rcvBytes = recv(s_fd, chunk, sizeof(chunk), 0)) >\n%s\n”, rcvBytes, loopRcvd, chunk);
		}
	}

	printf("Exiting receive_basic(s_fd %d) with loopRcvd %d \n\n", s_fd, loopRcvd);
	return totalRcvd;
}*/


/*
	Receive data in multiple chunks by checking a non-blocking socket
	Timeout in seconds
*/
int recv_timeout(int s, char *chunk, int timeout, int total)
{
	
	int size_recv , total_size= 0;
	struct timeval begin , now;
	double timediff;
	
	//make socket non blocking
	fcntl(s, F_SETFL, O_NONBLOCK);
	
	//beginning time
	gettimeofday(&begin , NULL);
	
	while(1)
	{
		gettimeofday(&now , NULL);
		
		//time elapsed in seconds
		timediff = (now.tv_sec - begin.tv_sec) + 1e-6 * (now.tv_usec - begin.tv_usec);

		if (total_size == total)
			break;
		
		//if you got some data, then break after timeout
		if( total_size > 0 && timediff > timeout )
		{
			break;
		}
		
		//if you got no data at all, wait a little longer, twice the timeout
		else if( timediff > timeout*2)
		{
			break;
		} 
		
		memset(chunk ,0 , MAXBUF);	//clear the variable
		if((size_recv =  recv(s , chunk , MAXBUF , 0) ) < 0)
		{
			//if nothing was received then we want to wait a little before trying again, 0.1 seconds
			usleep(100000);
		}
		else
		{
			total_size += size_recv;
			printf("%s" , chunk);
			//reset beginning time
			gettimeofday(&begin , NULL);
		}
	}
	
	return total_size;
}



int main(int argc , char *argv[])
{
	int socket_desc , client_sock , c , read_size;
	struct sockaddr_in server , client;
	char client_message[MAXBUF];
	
	//Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons( PORTNUMBER );
	
	//Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		//print the error message
		perror("bind failed. Error");
		return 1;
	}
	puts("bind done");
	
	//Listen
	listen(socket_desc , 3);
	
	//Accept and incoming connection
	puts("Waiting for incoming connections...");
	c = sizeof(struct sockaddr_in);
	
	//accept connection from an incoming client
	client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
	if (client_sock < 0)
	{
		perror("accept failed");
		return 1;
	}
	puts("Connection accepted");
	
	//Receive a message from client
	


	// recv
	int len = sizeof(client_message);
	char *p = client_message;
	printf("Len out of %d\n", len);

	while( len > 0 && (read_size = recv(client_sock , p , MAXBUF , 0)) > 0 )
	// while( (read_size = recv(client_sock , client_message , 400000 , 0)) > 0 )
	// 
	// while( (read_size = recv_timeout(client_sock , client_message, 2, 37940)) > 0 )
	{
		/*Extract the picture size given by: SIZE XXXX.
		 */
		p += read_size;
  		len =- read_size;
		int imageSize = -1;
		char ack[12] = "ACK_SIZE";
		char image[40000];
		int parsing = sscanf(client_message, "SIZE : %d", &imageSize);
		// client_message[strlen(client_message) - 2] = '\0';
		// printf("Clie Ms: %s", client_message);

		/* if(parsing == 1){
			printf("Image Size: %d\n", imageSize);
			strcpy(ack, "ACK_SIZE\n");
			//image = malloc(imageSize * sizeof(char));
		} else if((parsing = sscanf(client_message, "IMAGE : %s", image)) == 1) {
			printf("[*] CL IMAGE = %s --- ACK IMAGE %s\n", client_message, image);
			strcpy(ack, "ACK_IMAGE\n");
			adx_store_data("new_photo.png", client_message);
			//if((read_size = recv(client_sock , image , imageSize , 0)) > 0){
				
			//}
			// else
				//strcpy(ack, "NACK_IMAGE\n");
			
		}
		else
				strcpy(ack, "NACK\n");
		
		ack[strlen(ack) - 1] = '\0';

		//Send the message back to client*/
		write(client_sock , ack , strlen(ack));
		printf("Len Message = %ld\n", strlen(client_message));
		printf("Len P = %ld\n", strlen(p));
		// client_message[0] = '\0';
		// free(image);
	}

	if ( len > 0 || read_size < 0 ) {
  		// oops, something went wrong
		  puts("oops, something went wrong");
	}
	
	if(read_size == 0)
	{
		puts("Client disconnected");
		fflush(stdout);
	}
	else if(read_size == -1)
	{
		perror("recv failed");
	}
	
	return 0;
}