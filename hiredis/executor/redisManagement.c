// gcc example.c -o example -lhiredis
#include "executor.h"
redisContext* connectingToRedisDb(int argc, char **argv){
    redisContext *c;
    unsigned int isunix = 0;
    const char *hostname = (argc > 1) ? argv[1] : "127.0.0.1";

    if (argc > 2) {
        if (*argv[2] == 'u' || *argv[2] == 'U') {
            isunix = 1;
            /* in this case, host is the path to the unix socket */
            printf("Will connect to unix socket @%s\n", hostname);
        }
    }

    int port = (argc > 2) ? atoi(argv[2]) : 6379;

    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
    if (isunix) {
        c = redisConnectUnixWithTimeout(hostname, timeout);
    } else {
        c = redisConnectWithTimeout(hostname, port, timeout);
    }
    if (c == NULL || c->err) {
        if (c) {
            printf("Connection error: %s\n", c->errstr);
            redisFree(c);
        } else {
            printf("Connection error: can't allocate redis context\n");
        }
        exit(1);
    }
    return c;
}

// REturn 0 when the ping server occured with success; otherwise 1
int testRedisServer(redisContext *c){
    /* PING server */
    char redisResponse[5];
    redisReply *reply;
    reply = redisCommand(c,"PING");
    // printf("PING: %s\n", reply->str);
    strcpy(redisResponse, reply->str);
    freeReplyObject(reply);
    if(strcmp(redisResponse, "PONG") == 0)
        return 0;
    else 
        return 1;
}

// Send Command 
int sendRequest(int method){
    int returnValue = 0; //0 Success, 1 - Fail
    switch(method){
        case 0:

            break;
        case 1:

            break;
        default:
            returnValue = 1;
            break;
    }
    return returnValue;
}

//This function generates key for hashing
char *randstring(size_t length) { // length should be qualified as const if you follow a rigorous standard

    static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";    
    char *randomString;   // initializing to NULL isn't necessary as malloc() returns NULL if it couldn't allocate memory as requested

    if (length) {
        randomString = malloc(length +1); // I removed your `sizeof(char) * (length +1)` as sizeof(char) == 1, cf. C99

        if (randomString) {        
            for (int n = 0;n < length;n++) {        
                int key = rand() % (int) (sizeof(charset) -1);
                randomString[n] = charset[key];
            }

            randomString[length] = '\0';
        }
    }
    return randomString;
}