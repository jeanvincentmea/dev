#ifndef EXECUTOR_H_   /* Include guard */
#define EXECUTOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <hiredis/hiredis.h>
#include <sys/socket.h>
#include <arpa/inet.h>   
#include <unistd.h>  
#include <errno.h>

// gcc example.c -o example -lhiredis

redisContext* connectingToRedisDb(int argc, char **argv);
// REturn 0 when the ping server occured with success; otherwise 1
int testRedisServer(redisContext *c);

// Send Command 
int sendRequest(int method);

char *randstring(size_t length);



#endif // EXECUTOR_H_
